#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libsocket/libinetsocket.h>
#include <ctype.h>


FILE * connect_to_server();
void menu();
char get_choice();
void list_files(FILE *s);
void download(FILE *s);
void quit(FILE *s);

int main()
{
    // Connect
    FILE * s = connect_to_server();
    
    char choice;
    do{
        // Menu
        menu();
        
        // Get choice
        choice = get_choice();
        
        // Handle choice
        switch(choice)
        {
            case 'l':
            case 'L':
                list_files(s);
                break;
            
            case 'd':
            case 'D':
                download(s);
                break;
                
            case 'q':
            case 'Q':
                quit(s);
                exit(0);
                break;
                
            default:
                printf("Choice must be d, l, or q\n");
        }
    }while(choice != 'q'&& choice != 'Q');
    printf("We're here");
}

/*
 * Connect to server. Returns a FILE pointer that the
 * rest of the program will use to send/receive data.
 */
FILE * connect_to_server()
{  
    int sockfd = create_inet_stream_socket("runwire.com", "1234", LIBSOCKET_IPv4, 0);
    return fdopen(sockfd, "r+");
}


/*
 * Display menu of choices.
 */
void menu()
{
    printf("List files\n");
    printf("Download a file\n");
    printf("Quit\n");
    printf("\n");
}

/*
 * Get the menu choice from the user. Allows the user to
 * enter up to 100 characters, but only the first character
 * is returned.
 */
char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
    return buf[0];
    
}

/*
 * Display a file list to the user.
 */
void list_files(FILE *s)
{
    printf("List of files on the server:\n");
    fprintf(s, "LIST\n");
    char line[1000];
    while (fgets(line, 1000, s) != NULL){
        printf("%s", line);
        if (strlen(line)<3) break;
    }
}

/*
 * Download a file.
 * Prompt the user to enter a filename.
 * Download it from the server and save it to a file with the
 * same name.
 */
void download(FILE *s)
{
    FILE * f;
    char choice[80];
    printf("Which file? ");
    fgets (choice, 80, stdin);
    
    f = fopen(choice, "wb");
    
    char buf[1000];
    int size;
    fprintf(s, "SIZE %s", choice);
    while (fgets(buf, 1000, s) != NULL){
        sscanf(buf,"+OK %d",&size);
        break;
    }
    printf("Size of this file is %d\n", size);
    fprintf(s, "GET %s", choice);
    unsigned char data[100];
    int so_far = 0;
    int got;
    int count = 0;
    while (1)
    {
        //read socket 100 bytes at a time unless less than 100 bytes present in socket
        if(((size+4)-so_far) < 100){
            //if less than 100 bytes in socket, read all there is
            got = fread(data, sizeof(unsigned char), ((size+4)-so_far), s);
        }
        else{
            got = fread(data, sizeof(unsigned char), 100, s);
        }
        fwrite(data, sizeof(unsigned char), got, f);
        so_far += got;
        //Progress bar
        if(((double)so_far/(double)size)*100 >= ((double)count/10)*100){
            while((((double)so_far/(double)size)*100) >= (((double)(count+1)/10)*100)){
                count++;
                printf("#");
                fflush(stdout);
            }
        }
        
        if(so_far >= size){
            break;
        }
    }
    fclose(f);
    printf("\n");

}



/* 
 * Close the connection to the server.
 */
void quit(FILE *s)
{
    fclose(s);
}